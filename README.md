
Contents
---------

[[_TOC_]]

----------
# Quality Assurance

## Purpose
The purpose of this repository is to centralize the tools used for the quality-assurance of the components of the .stat-suite. 
- Currently the quality-assurance covers two areas:
	- **Performance testing** 
	- **Security testing**

### Performance testing
The performance tests have been configured in the GitLab CI pipelines, to automatically test the nsiws and transfer-services during the development process.
- These tests include performance testing for imports and extractons of data and structures.
> A detailed information about the performance tests can be found in the [README.md](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/blob/master/PerformanceTests/README.md) in the PerformanceTests folder, in this repository.
- There are two pipelines configured to run two performance tests
	- **Triggered jobs** 
	- **Daily scheduled jobs**

### Security testing
The dynamic application/blackbox security tests (SAST) are run using [Netsparker](netsparkercloud.com). 
Only the Gitlab repositories containing public interfaces are scanned, which are listed in the [yaml](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/blob/master/.netsparker-security-test-gitlab-ci.yml) pipeline description file.
Each .stat-suite Gitlab repository is scanned separatly and the result can be viewed in the Netsparker web GUI.
For more information on how to access scanned reports, please contact any Gitlab user withn the SIS-CC group that as a owner role.  

## CI pipeline setup

### Pipeline triggers
The Quality assurance CI pipeline gets triggered by either when a commit is done on the develop branch in one of the .Stat-suite component repositories or as a scheduled job. 
More details for the Performance tests is documented separatly [here](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/blob/master/PerformanceTests/README.md). 

###  Performance tests
The triggered performance test jobs contain light performance tests for imports and exports of data and structures.
- The jobs are configured to be triggered by a merge of a feature branch into development in the [dotstatsuite-core-transfer repository](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer) 
- The objective of this tests is to:
	- Provide a sanity check every time there are new changes to the NSI-WS and the transfer-service.
	- Verify that these systems doesn't throw any errors when under minimal load.
- The file **.smoke-performance-test-gitlab-ci** describes the steps to be executed.
    - The tests are run against the "qa:reset" space of the [nsi-ws QA environment](http://nsi-reset-qa.siscc.org/) and the [transfer-service QA environment](http://transfer-qa.siscc.org/)
    - The summary of the result of each test run can be found as a downloadable json file in the [job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html).

###### Scheduled CI jobs 
The scheduled jobs contain extensive performance tests for imports and exports of data and structures. 
- They are configured to run every weekend to avoid affecting the users, by the high traffic caused by the performance tests.
- These tests can be launched manually, if needed, from the [list of scheduled pipelines](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/pipeline_schedules)
- The objective of this tests is to:
	- Assess the current performance of the NSI-WS and the transfer-service under typical and peak load.
	- Make sure that these systems are continuously meeting the performance standards as changes are made to the system (code).
- The file **.extended-performance-test-gitlab-ci.yml** describes the steps to be executed.
    - The tests are run against the "qa:stable" space of the [nsi-ws QA environment](http://nsi-stable-qa.siscc.org/) and the [transfer-service QA environment](http://transfer-qa.siscc.org/)
    - The summary of the result of each test run can be found as a downloadable json file in the [job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html).




