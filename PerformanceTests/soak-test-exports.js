/******************
	This test provies scenario for soak testing a NSI-WS:
		1.- The soak test uncovers performance and reliability issues stemming from a system being under pressure for an extended period.
		2.- Reliability issues typically relate to bugs, memory leaks, insufficient storage quotas, incorrect configuration or infrastructure failures. 
		3.- Performance issues typically relate to incorrect database tuning, memory leaks, resource leaks or a large amount of data.
	
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';

//Default NSI-WS host url
let NSIWS_HOSTNAME = "http://127.0.0.1:81";
if(typeof __ENV.NSIWS_HOSTNAME !== 'undefined'){
	NSIWS_HOSTNAME = __ENV.NSIWS_HOSTNAME;
}
	
let PID = 3497345;
if(typeof __ENV.K6_PID !== 'undefined'){
	PID = __ENV.K6_PID;
}
	
let TEST_NAME = "Soak-test-exports";
if(typeof __ENV.TEST_NAME !== 'undefined'){
	TEST_NAME = __ENV.TEST_NAME;
}

let INPUT_FILE = "./Resources/test-cases-exports.json";
if(typeof __ENV.TEST_CASES_FILE !== 'undefined'){
	INPUT_FILE = __ENV.TEST_CASES_FILE;
}

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
		
//NSI-WS available data response types
const SUPPORTED_RESPONSE_FORMATS =["csv", "json", "xml"];
	
export let options = {
	ext: {
		loadimpact: {
		  projectID: PID, //k6 CLOUD project id 
		  name: TEST_NAME
		}
	},
	//Target = number of max users to scale to
	/*
	-We recommend you to configure your soak test at about 80% capacity of your system. 
	-If your system can handle a maximum of 500 simultaneous users, you should configure your soak test to 400 VUs.
	*/
	stages: [
		{ duration: '2m', target: 32 }, // ramp up to 32 users
		{ duration: '50m', target: 32 }, // stay at 32 for ~50 min
		{ duration: '2m', target: 0 }, // scale down. (optional)
	],
	//Discard the response bodies to lessen the amount of memmory required by the testing machine.
	discardResponseBodies: true,

};

export function setup() {
	// 2. setup code

	//check that the nsi-ws hostname is available
	let healhCheck = http.get(`${NSIWS_HOSTNAME}/health`);
	if (healhCheck.status !== 200){
		fail(`Error: the NSI-WS {${NSIWS_HOSTNAME}/health} is not responding.`);
	}
	
	console.log(`Testing the NSI-WS {${NSIWS_HOSTNAME}}`);
}

export default function() {
	//Generate random query 
	let testQuery = TEST_CASES[Math.floor(Math.random() * TEST_CASES.length)];
    //console.log('Query: ', testQuery.query);
	
	let params = { headers: {'Accept-Ecoding': 'gzip, deflate', 'Content-Type':'application/xml'}};
		
	group(`Query type ${testQuery.queryType}`, function (){
			
		//structure query			
		if(testQuery.queryType==="structure"){
			params = { headers: {'Accept-Ecoding': 'gzip, deflate', 'Content-Type':'application/vnd.sdmx.structure+json; version=1.0; charset=utf-8'}};
			group(`Struc type ${testQuery.structureType}`, function (){
				let response = http.get(
					`${NSIWS_HOSTNAME}${testQuery.query}`, //URL
					params, //headers 
				);	
								
				check(response, {
					"status is 200": (r) => r.status == 200
				});
			});
		}
		
		//data query
		else{
			//Generate random format 
			let responseFormat = SUPPORTED_RESPONSE_FORMATS[Math.floor(Math.random() * SUPPORTED_RESPONSE_FORMATS.length)];
			if(typeof __ENV.RESPONSE_FORMAT !== 'undefined')
				responseFormat = __ENV.RESPONSE_FORMAT;
			//console.log('Format: ', responseFormat);
					
			params.headers.Accept=`application/vnd.sdmx.data+${responseFormat}`;
			if(responseFormat==="xml")
				params.headers.Accept=`application/vnd.sdmx.genericdata+${responseFormat}`;
				
			var expectedResponseStatusCode = 200;
			//has range headers
			if ('range' in testQuery) {
				params.headers.Range=testQuery.range;
				expectedResponseStatusCode = 206;//partial content
			}
			//add tags
			params.tags={'datasetSize':testQuery.datasetSize}
			
			group(`Format ${responseFormat}`, function (){
				group(`Size ${testQuery.responseSize}`, function (){
					let response = http.get(
						`${NSIWS_HOSTNAME}${testQuery.query}`, //URL
						params, //headers 
					);	
					var msg= `status is ${expectedResponseStatusCode}`;
					check(response, {
						msg: (r) => r.status == expectedResponseStatusCode
					});
				});
			});
		}
	});
	//wait 1 second after each test iteration
	sleep(1);
}
