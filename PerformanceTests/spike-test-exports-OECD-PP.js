/******************
	This test provies scenario for spike testing a NSI-WS:
		1.- Spike test is a variation of a stress test, but it does not gradually increase the load, instead it spikes to extreme load over a very short window of time
		2.- While a stress test allows the SUT (System Under Test) to gradually scale up its infrastructure, a spike test does not.
		3.- Determine how your system will perform under a sudden surge of traffic.
		4.- Determine if your system will recover once the traffic has subsided.
	SUCCESS/FAILUER
		Excellent: system performance is not degraded during the surge of traffic. Response time is similar during low traffic and high traffic.
		Good: Response time is slower, but the system does not produce any errors. All requests are handled.
		Poor: System produces errors during the surge of traffic, but recovers to normal after the traffic subsides.
		Bad: System crashes, and does not recover after the traffic has subsided.
*******************/
import http from 'k6/http';
import { Rate, Trend } from 'k6/metrics';
import { check, sleep, group, fail } from 'k6';
	
//Default NSI-WS host url
let NSIWS_HOSTNAME = "http://127.0.0.1:81";
if(typeof __ENV.NSIWS_HOSTNAME !== 'undefined'){
	NSIWS_HOSTNAME = __ENV.NSIWS_HOSTNAME;
}
	
let PID = 3497345;
if(typeof __ENV.K6_PID !== 'undefined'){
	PID = __ENV.K6_PID;
}
		
let TEST_NAME = "Spike-test-exports";
if(typeof __ENV.TEST_NAME !== 'undefined'){
	TEST_NAME = __ENV.TEST_NAME;
}

let INPUT_FILE = "./Resources/test-cases-exports.json";
if(typeof __ENV.TEST_CASES_FILE !== 'undefined'){
	INPUT_FILE = __ENV.TEST_CASES_FILE;
}

//Keycloak parameters
let ACCESS_TOKEN_URL= "https://dotstat-access-pp.oecd.org/auth/realms/OECD/protocol/openid-connect/token";
if(typeof __ENV.KEYCLOAK_AT_URL !== 'undefined'){
	ACCESS_TOKEN_URL = __ENV.KEYCLOAK_AT_URL;
}
let currentAccessToken = "";
let accessTokenExpiry ="";

//Keycloak credentials
let USERNAME="";
if(typeof __ENV.USERNAME !== 'undefined'){
	USERNAME = __ENV.USERNAME;
}

let PASSWORD="";
if(typeof __ENV.PASSWORD !== 'undefined'){
	PASSWORD = __ENV.PASSWORD;
}

//login to keycloak?
let getToken =true;
if(typeof __ENV.USERNAME === 'undefined' || typeof __ENV.PASSWORD === 'undefined'){
	getToken=false;
}

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));

//NSI-WS available data response types
const SUPPORTED_RESPONSE_FORMATS =["csv", "json", "xml"];
if(typeof __ENV.RESPONSE_FORMAT !== "undefined"){
	if(SUPPORTED_RESPONSE_FORMATS.indexOf(__ENV.RESPONSE_FORMAT) < 0) 
		fail(`The response format '${__ENV.RESPONSE_FORMAT}', is not supported.`);
}
	
export let options = {
	ext: {
		loadimpact: {
		  projectID: PID, //k6 CLOUD project id 
		  name: TEST_NAME
		}
	},
	//Target = number of max users to scale to
	stages: [
		{ duration: '10s', target: 30 }, // below normal load
		{ duration: '1m',  target: 30 },
		{ duration: '10s', target: 400 }, // spike to 400 users
		{ duration: '3m',  target: 400 }, // stay at 400 for 3 minutes
		{ duration: '10s', target: 30 }, // scale down. Recovery stage.
		{ duration: '3m',  target: 30 },
		{ duration: '10s', target: 0 },
	],
	thresholds: {
		'checks': ['rate>0.94'], // more than 94% success rate
		
        "http_req_duration{group:::Query type structure::Struc type agencyscheme}":   	["avg<1000"],
        "http_req_duration{group:::Query type structure::Struc type categoryscheme}": 	["avg<1000"],
        "http_req_duration{group:::Query type structure::Struc type categorisation}": 	["avg<1000"],
        "http_req_duration{group:::Query type structure::Struc type codelist}":       	["avg<1000"],
        "http_req_duration{group:::Query type structure::Struc type conceptscheme}":  	["avg<1000"],
        "http_req_duration{group:::Query type structure::Struc type contentconstraint}":["avg<1000"],
        "http_req_duration{group:::Query type structure::Struc type dataflow}":       	["avg<60000"],
        "http_req_duration{group:::Query type structure::Struc type datastructure}":  	["avg<60000"],		
        "http_req_duration{group:::Query type structure::Struc type hierarchicalcodelist}": ["avg<1000"],		
        "http_req_duration{group:::Query type structure::Struc type metadataflow}":   	["avg<1000"],	
        "http_req_duration{group:::Query type structure::Struc type metadatastructure}":["avg<1000"],	
        "http_req_duration{group:::Query type structure::Struc type structureset}":   	["avg<1000"],	
				
        "http_req_duration{group:::Query type data::Format xml::Size extraSmall}": 	["avg<20000"],
        "http_req_duration{group:::Query type data::Format xml::Size small}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format xml::Size medium}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format xml::Size large}": 		["avg<25000"],
        "http_req_duration{group:::Query type data::Format xml::Size extraLarge}": 	["avg<65000"],
		
        "http_req_duration{group:::Query type data::Format json::Size extraSmall}": ["avg<20000"],
        "http_req_duration{group:::Query type data::Format json::Size small}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format json::Size medium}": 	["avg<20000"],
        "http_req_duration{group:::Query type data::Format json::Size large}": 		["avg<25000"],
        "http_req_duration{group:::Query type data::Format json::Size extraLarge}": ["avg<65000"],
		
        "http_req_duration{group:::Query type data::Format csv::Size extraSmall}": 	["avg<20000"],
        "http_req_duration{group:::Query type data::Format csv::Size small}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format csv::Size medium}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format csv::Size large}": 		["avg<25000"],
        "http_req_duration{group:::Query type data::Format csv::Size extraLarge}": 	["avg<65000"],
		
        "http_req_duration{group:::Query type data::Format xml::Size extraSmall_paginated}": 	["avg<20000"],
        "http_req_duration{group:::Query type data::Format xml::Size small_paginated}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format xml::Size medium_paginated}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format xml::Size large_paginated}": 		["avg<25000"],
        "http_req_duration{group:::Query type data::Format xml::Size extraLarge_paginated}": 	["avg<65000"],
		
        "http_req_duration{group:::Query type data::Format json::Size extraSmall_paginated}":   ["avg<20000"],
        "http_req_duration{group:::Query type data::Format json::Size small_paginated}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format json::Size medium_paginated}": 	    ["avg<20000"],
        "http_req_duration{group:::Query type data::Format json::Size large_paginated}": 		["avg<25000"],
        "http_req_duration{group:::Query type data::Format json::Size extraLarge_paginated}":   ["avg<65000"],
		
        "http_req_duration{group:::Query type data::Format csv::Size extraSmall_paginated}": 	["avg<20000"],
        "http_req_duration{group:::Query type data::Format csv::Size small_paginated}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format csv::Size medium_paginated}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format csv::Size large_paginated}": 		["avg<25000"],
        "http_req_duration{group:::Query type data::Format csv::Size extraLarge_paginated}": 	["avg<65000"],
		
        "http_req_duration{datasetSize:extraSmall}":["avg<20000"],
        "http_req_duration{datasetSize:small}": 	["avg<20000"],
        "http_req_duration{datasetSize:medium}": 	["avg<25000"],
        "http_req_duration{datasetSize:large}": 	["avg<30000"],
        "http_req_duration{datasetSize:extraLarge}":["avg<65000"],
		
        "http_req_duration{datasetSize:extraSmall_paginated}":["avg<20000"],
        "http_req_duration{datasetSize:small_paginated}": 	  ["avg<20000"],
        "http_req_duration{datasetSize:medium_paginated}": 	  ["avg<25000"],
        "http_req_duration{datasetSize:large_paginated}": 	  ["avg<30000"],
        "http_req_duration{datasetSize:extraLarge_paginated}":["avg<65000"],	
	},	
	//Discard the response bodies to lessen the amount of memmory required by the testing machine.
	discardResponseBodies: true,

};

export function setup() {
	// 2. setup code

	//check that the nsi-ws hostname is available
	let healhCheck = http.get(`${NSIWS_HOSTNAME}/health`);
	if (healhCheck.status !== 200){
		fail(`Error: the NSI-WS {${NSIWS_HOSTNAME}/health} is not responding.`);
	}
	
	console.log(`Testing the NSI-WS {${NSIWS_HOSTNAME}}`);
}

export default function() {
	//Generate random query 
	let testQuery = TEST_CASES[Math.floor(Math.random() * TEST_CASES.length)];
    //console.log('Query: ', testQuery.query);
	
	//Get new access token if current token has expired
	TryToGetNewAccessToken();
	let params = { headers: {'Accept-Ecoding': 'gzip, deflate',	'Authorization': `Bearer ${currentAccessToken}`}	};
		
	group(`Query type ${testQuery.queryType}`, function (){
			
		//structure query			
		if(testQuery.queryType==="structure"){
			params = { headers: {'Accept-Ecoding': 'gzip, deflate', 'Content-Type':'application/vnd.sdmx.structure+json; version=1.0; charset=utf-8',	'Authorization': `Bearer ${currentAccessToken}`}};
			group(`Struc type ${testQuery.structureType}`, function (){
				let response = http.get(
					`${NSIWS_HOSTNAME}${testQuery.query}`, //URL
					params, //headers 
				);	
								
				check(response, {
					"status is 200": (r) => r.status == 200
				});
			});
		}
		//data query
		else{
			//Generate random format 
			let responseFormat = SUPPORTED_RESPONSE_FORMATS[Math.floor(Math.random() * SUPPORTED_RESPONSE_FORMATS.length)];
			if(typeof __ENV.RESPONSE_FORMAT !== 'undefined')
				responseFormat = __ENV.RESPONSE_FORMAT;
			//console.log('Format: ', responseFormat);
			
			params.headers.Accept=`application/vnd.sdmx.data+${responseFormat}`;
	
			if(responseFormat==="xml")
				params.headers.Accept=`application/vnd.sdmx.genericdata+${responseFormat}`;
	
			group(`Format ${responseFormat}`, function (){
				group(`Size ${testQuery.responseSize}`, function (){
					let response = http.get(
						`${NSIWS_HOSTNAME}${testQuery.query}`, //URL
						{tags:{datasetSize:testQuery.datasetSize}},//tags
						params, //headers 
					);	
				
					check(response, {
						"status is 200": (r) => r.status == 200
					});
				});
			});
		}
	});
	//wait 1 second after each test iteration
	sleep(1);
}
