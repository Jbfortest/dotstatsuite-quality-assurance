/******************
	This test provies scenario for smoke testing data imports to the nsi-ws:
		1.- Assess the current performance of the nsi-ws for basic benchmark tests.
		2.- Make sure that the nsi-ws is continuously meeting the performance standards as changes are made to the system (code).
	
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';
import { Rate, Trend } from 'k6/metrics';
import { TryToGetNewAccessToken } from './Resources/authenticate.js';

//Default transfer-service host url
let BASE_URL = "http://127.0.0.1:81";
if(typeof __ENV.NSIWS_HOSTNAME !== 'undefined'){
	BASE_URL = __ENV.NSIWS_HOSTNAME;
}
	
let PID = 3497345;
if(typeof __ENV.K6_PID !== 'undefined'){
	PID = __ENV.K6_PID;
}

//K6 cloud test name
let TEST_NAME = "smoke-test-structure-imports";
if(typeof __ENV.TEST_NAME !== 'undefined'){
	TEST_NAME = __ENV.TEST_NAME;
}
	
//Keycloak parameters
let ACCESS_TOKEN_URL= "https://keycloak.siscc.org/auth/realms/OECD/protocol/openid-connect/token";
if(typeof __ENV.KEYCLOAK_AT_URL !== 'undefined'){
	ACCESS_TOKEN_URL = __ENV.KEYCLOAK_AT_URL;
}
let currentAccessToken = "";
let accessTokenExpiry ="";

//Keycloak credentials
let USERNAME="";
if(typeof __ENV.USERNAME !== 'undefined'){
	USERNAME = __ENV.USERNAME;
}

let PASSWORD="";
if(typeof __ENV.PASSWORD !== 'undefined'){
	PASSWORD = __ENV.PASSWORD;
}

//login to keycloak?
let getToken =true;
if(typeof __ENV.USERNAME === 'undefined' || typeof __ENV.PASSWORD === 'undefined'){
	getToken=false;
}

let INPUT_FILE = "./Resources/test-cases-structure-imports.json";
if(typeof __ENV.TEST_CASES_FILE !== 'undefined'){
	INPUT_FILE = __ENV.TEST_CASES_FILE;
}

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
//Open input files
for(let testCase in TEST_CASES){
	TEST_CASES[testCase].File = open(`./Resources/Structures/${TEST_CASES[testCase].structureFile}`, "b");
}

export let options = {
	setupTimeout: "10s",
	ext: {
		loadimpact: {
		  projectID: PID, //k6 CLOUD project id 
		  name: TEST_NAME
		}
	},
	thresholds: {
		"checks": ['rate>0.99'], // more than 99% success rate on import requests
        "http_req_duration{group:::Structure type multiple}": ["p(95)<50000"],			
        "http_req_duration{group:::Structure type agencyscheme}": ["p(95)<30000"],
        "http_req_duration{group:::Structure type categoryscheme}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type categorisation}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type codelist}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type conceptscheme}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type contentconstraint}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type dataflow}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type datastructure}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type hierarchicalcodelist}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type metadataflow}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type metadatastructure}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type structureset}": ["p(95)<30000"],			
	},
	iterations: 1,
	vus: 1,

};

export function setup() {
	// 2. setup code
	
	//check that the transfer-service hostname is available
	let healhCheck = http.get(`${BASE_URL}/health`);
	if (healhCheck.status !== 200){
		fail(`Error: the nsi-ws {${BASE_URL}/health} is not responding.`);
	}
	console.log(`Testing the nsi-ws {${BASE_URL}} version ${healhCheck.json().service.details.version}`);
	
}

export default function() {
	//Get new access token if current token has expired
	TryToGetNewAccessToken();
	
	let headers= {
		'Accept':'*/*',
		'Authorization': `Bearer ${currentAccessToken}`, 
		'Content-Type': 'application/xml'
	};
	
	//Submit data import requests
	for(let testCase in TEST_CASES){
		
		console.log(`Importing file: ${TEST_CASES[testCase].structureFile}`);	
		
		group(`Structure type ${TEST_CASES[testCase].structureType}`, function (){
			var url=`${BASE_URL}/rest/structure`;
			var data= TEST_CASES[testCase].File;
			var res = http.post(url, data, {headers: headers});

			console.log(`import status:${res.status}`);
			
			check(res, {
				'is status 201(Created) or 207(Multi-status)': (r) => r.status === 201||r.status === 207
			});
		});
		
		//wait 1 second after each test iteration
		sleep(1);
	}
}