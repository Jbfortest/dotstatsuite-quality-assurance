/******************
	This test provies scenario for load testing a NSI-WS:
		1.- Assess the current performance of the NSI-WS under typical and peak load.
		2.- Make sure that the NSI-WS is continuously meeting the performance standards as changes are made to the system (code and infrastructure).
	
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';
import { Rate, Trend } from 'k6/metrics';
import { TryToGetNewAccessToken } from './Resources/authenticate.js';

//Default NSI-WS host url
let NSIWS_HOSTNAME = "http://127.0.0.1:81";
if(typeof __ENV.NSIWS_HOSTNAME !== 'undefined'){
	NSIWS_HOSTNAME = __ENV.NSIWS_HOSTNAME;
}
	
let PID = 3497345;
if(typeof __ENV.K6_PID !== 'undefined'){
	PID = __ENV.K6_PID;
}

let TEST_NAME = "Load-test-exports";
if(typeof __ENV.TEST_NAME !== 'undefined'){
	TEST_NAME = __ENV.TEST_NAME;
}

//Keycloak parameters
let ACCESS_TOKEN_URL= "https://dotstat-access-pp.oecd.org/auth/realms/OECD/protocol/openid-connect/token";
if(typeof __ENV.KEYCLOAK_AT_URL !== 'undefined'){
	ACCESS_TOKEN_URL = __ENV.KEYCLOAK_AT_URL;
}
let currentAccessToken = "";
let accessTokenExpiry ="";

//Keycloak credentials
let USERNAME="";
if(typeof __ENV.USERNAME !== 'undefined'){
	USERNAME = __ENV.USERNAME;
}

let PASSWORD="";
if(typeof __ENV.PASSWORD !== 'undefined'){
	PASSWORD = __ENV.PASSWORD;
}

//login to keycloak?
let getToken =true;
if(typeof __ENV.USERNAME === 'undefined' || typeof __ENV.PASSWORD === 'undefined'){
	getToken=false;
}

//NSI-WS available data response types
const SUPPORTED_RESPONSE_FORMATS =["csv", "json", "xml"];
if(typeof __ENV.RESPONSE_FORMAT !== "undefined"){
	if(SUPPORTED_RESPONSE_FORMATS.indexOf(__ENV.RESPONSE_FORMAT) < 0) 
		fail(`The response format '${__ENV.RESPONSE_FORMAT}', is not supported.`);
}

let INPUT_FILE = "./Resources/test-cases-exports.json";
if(typeof __ENV.TEST_CASES_FILE !== 'undefined'){
	INPUT_FILE = __ENV.TEST_CASES_FILE;
}

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
	
export let options = {
	ext: {
		loadimpact: {
		  projectID: PID, //k6 CLOUD project id 
		  name: TEST_NAME
		}
	},
	//Target = number of max users to scale to
	stages: [
		{ duration: '2m', target: 50 }, // simulate ramp-up of traffic from 1 to 50 users over 2 minutes.
		{ duration: '4m', target: 50 }, // stay at 50 users for 4 minutes
		{ duration: '1m', target: 80 }, // ramp-up to 80 users over 1 minutes (peak hour starts)
		{ duration: '40s', target: 80 }, // stay at 80 users for short amount of time (peak hour)
		{ duration: '1m', target: 50 }, // ramp-down to 50 users over 1 minutes (peak hour ends)
		{ duration: '4m', target: 50 }, // continue at 50 for additional 4 minutes
		{ duration: '2m', target: 0 }, // ramp-down to 0 users
	],
	thresholds: {
		'checks': ['rate>0.94'], // more than 94% success rate
		
        "http_req_duration{group:::Query type structure::Struc type agencyscheme}":   	["avg<1000"],
        "http_req_duration{group:::Query type structure::Struc type categoryscheme}": 	["avg<1000"],
        "http_req_duration{group:::Query type structure::Struc type categorisation}": 	["avg<1000"],
        "http_req_duration{group:::Query type structure::Struc type codelist}":       	["avg<1000"],
        "http_req_duration{group:::Query type structure::Struc type conceptscheme}":  	["avg<1000"],
        "http_req_duration{group:::Query type structure::Struc type contentconstraint}":["avg<1000"],
        "http_req_duration{group:::Query type structure::Struc type dataflow}":       	["avg<60000"],
        "http_req_duration{group:::Query type structure::Struc type datastructure}":  	["avg<60000"],		
        "http_req_duration{group:::Query type structure::Struc type hierarchicalcodelist}": ["avg<1000"],		
        "http_req_duration{group:::Query type structure::Struc type metadataflow}":   	["avg<1000"],	
        "http_req_duration{group:::Query type structure::Struc type metadatastructure}":["avg<1000"],	
        "http_req_duration{group:::Query type structure::Struc type structureset}":   	["avg<1000"],	
				
        "http_req_duration{group:::Query type data::Format xml::Size extraSmall}": 	["avg<20000"],
        "http_req_duration{group:::Query type data::Format xml::Size small}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format xml::Size medium}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format xml::Size large}": 		["avg<25000"],
        "http_req_duration{group:::Query type data::Format xml::Size extraLarge}": 	["avg<65000"],
		
        "http_req_duration{group:::Query type data::Format json::Size extraSmall}": ["avg<20000"],
        "http_req_duration{group:::Query type data::Format json::Size small}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format json::Size medium}": 	["avg<20000"],
        "http_req_duration{group:::Query type data::Format json::Size large}": 		["avg<25000"],
        "http_req_duration{group:::Query type data::Format json::Size extraLarge}": ["avg<65000"],
		
        "http_req_duration{group:::Query type data::Format csv::Size extraSmall}": 	["avg<20000"],
        "http_req_duration{group:::Query type data::Format csv::Size small}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format csv::Size medium}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format csv::Size large}": 		["avg<25000"],
        "http_req_duration{group:::Query type data::Format csv::Size extraLarge}": 	["avg<65000"],
		
        "http_req_duration{group:::Query type data::Format xml::Size extraSmall_paginated}": 	["avg<20000"],
        "http_req_duration{group:::Query type data::Format xml::Size small_paginated}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format xml::Size medium_paginated}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format xml::Size large_paginated}": 		["avg<25000"],
        "http_req_duration{group:::Query type data::Format xml::Size extraLarge_paginated}": 	["avg<65000"],
		
        "http_req_duration{group:::Query type data::Format json::Size extraSmall_paginated}":   ["avg<20000"],
        "http_req_duration{group:::Query type data::Format json::Size small_paginated}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format json::Size medium_paginated}": 	    ["avg<20000"],
        "http_req_duration{group:::Query type data::Format json::Size large_paginated}": 		["avg<25000"],
        "http_req_duration{group:::Query type data::Format json::Size extraLarge_paginated}":   ["avg<65000"],
		
        "http_req_duration{group:::Query type data::Format csv::Size extraSmall_paginated}": 	["avg<20000"],
        "http_req_duration{group:::Query type data::Format csv::Size small_paginated}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format csv::Size medium_paginated}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format csv::Size large_paginated}": 		["avg<25000"],
        "http_req_duration{group:::Query type data::Format csv::Size extraLarge_paginated}": 	["avg<65000"],
		
        "http_req_duration{datasetSize:extraSmall}":["avg<20000"],
        "http_req_duration{datasetSize:small}": 	["avg<20000"],
        "http_req_duration{datasetSize:medium}": 	["avg<25000"],
        "http_req_duration{datasetSize:large}": 	["avg<30000"],
        "http_req_duration{datasetSize:extraLarge}":["avg<65000"],
		
        "http_req_duration{datasetSize:extraSmall_paginated}":["avg<20000"],
        "http_req_duration{datasetSize:small_paginated}": 	  ["avg<20000"],
        "http_req_duration{datasetSize:medium_paginated}": 	  ["avg<25000"],
        "http_req_duration{datasetSize:large_paginated}": 	  ["avg<30000"],
        "http_req_duration{datasetSize:extraLarge_paginated}":["avg<65000"],	
	},	
	//Discard the response bodies to lessen the amount of memmory required by the testing machine.
	discardResponseBodies: true,

};

export function setup() {
	// 2. setup code

	//check that the nsi-ws hostname is available
	let healhCheck = http.get(`${NSIWS_HOSTNAME}/health`);
	if (healhCheck.status !== 200){
		fail(`Error: the NSI-WS {${NSIWS_HOSTNAME}/health} is not responding.`);
	}
	
	console.log(`Testing the NSI-WS {${NSIWS_HOSTNAME}}`);
}

export default function() {
	//Generate random query 
	let testQuery = TEST_CASES[Math.floor(Math.random() * TEST_CASES.length)];
    //console.log('Query: ', testQuery.query);
	
	//Get new access token if current token has expired
	TryToGetNewAccessToken();
	
		let params = { 
			headers: {'Accept-Ecoding': 'gzip, deflate',	'Authorization': `Bearer ${currentAccessToken}`},
			responseType: 'none'
		};
	//let params = { headers: {'Accept-Ecoding': 'gzip, deflate',	'Authorization': `Bearer ${currentAccessToken}`}};
		
	group(`Query type ${testQuery.queryType}`, function (){
			
		//structure query			
		if(testQuery.queryType==="structure"){
				let headers= {
					'Accept-Ecoding': 'gzip, deflate',
					'Authorization': `Bearer ${currentAccessToken}`, 
					'Content-Type':'application/vnd.sdmx.structure+json; version=1.0; charset=utf-8'
				};
			//params = { headers: {'Accept-Ecoding': 'gzip, deflate', 'Content-Type':'application/vnd.sdmx.structure+json; version=1.0; charset=utf-8',	'Authorization': `Bearer ${currentAccessToken}`}};
			group(`Struc type ${testQuery.structureType}`, function (){
				let response = http.get(
					`${NSIWS_HOSTNAME}${testQuery.query}`, //URL
						{headers: headers}, {responseType: 'none'}, //headers 
					//params, //headers 
				);	
								
				check(response, {
					"status is 200": (r) => r.status == 200
				});
			});
		}
		//data query
		else{
			//Generate random format 
			let responseFormat = SUPPORTED_RESPONSE_FORMATS[Math.floor(Math.random() * SUPPORTED_RESPONSE_FORMATS.length)];
			if(typeof __ENV.RESPONSE_FORMAT !== 'undefined')
				responseFormat = __ENV.RESPONSE_FORMAT;
			//console.log('Format: ', responseFormat);
			
					let headers= {
						'Accept-Ecoding': 'gzip, deflate',
						'Authorization': `Bearer ${currentAccessToken}`, 
						'Content-Type':'application/vnd.sdmx.structure+json; version=1.0; charset=utf-8',
						'Accept':`application/vnd.sdmx.data+${responseFormat}`
					};
			//params.headers.Accept=`application/vnd.sdmx.data+${responseFormat}`;
			//params.headers.Authorization=`Bearer ${currentAccessToken}`;
	
			if(responseFormat==="xml"){
						
						let headers= {
							'Accept-Ecoding': 'gzip, deflate',
							'Authorization': `Bearer ${currentAccessToken}`,
							'Content-Type':'application/xml',
							'Accept':`application/vnd.sdmx.genericdata+${responseFormat}`
						};
					}
				//params.headers.Accept=`application/vnd.sdmx.genericdata+${responseFormat}`;
	
			group(`Format ${responseFormat}`, function (){
				group(`Size ${testQuery.responseSize}`, function (){
					let response = http.get(
						`${NSIWS_HOSTNAME}${testQuery.query}`, //URL
						{tags:{datasetSize:testQuery.datasetSize}, headers: headers, responseType: 'none'} //headers 
					);	
				
					check(response, {
						"status is 200": (r) => r.status == 200
					});
				});
			});
		}
	});
	//wait 1 second after each test iteration
	sleep(1);
}
