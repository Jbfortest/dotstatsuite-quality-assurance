/******************
	This test provies scenario for smoke testing data imports to the transfer-service:
		1.- Assess the current performance of the transfer-service for basic benchmark tests.
		2.- Make sure that the transfer-service is continuously meeting the performance standards as changes are made to the system (code).
	
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';
import { Rate, Trend } from 'k6/metrics';
import { TryToGetNewAccessToken } from './Resources/authenticate.js';

//Default transfer-service host url
let BASE_URL = "http://127.0.0.1:93";
if(typeof __ENV.TRANSFER_SERVICE_HOSTNAME !== 'undefined'){
	BASE_URL = __ENV.TRANSFER_SERVICE_HOSTNAME;
}

let PID = 3497348;
if(typeof __ENV.K6_PID !== 'undefined'){
	PID = __ENV.K6_PID;
}

//K6 cloud test name
let TEST_NAME = "smoke-test-data-imports";
if(typeof __ENV.TEST_NAME !== 'undefined'){
	TEST_NAME = __ENV.TEST_NAME;
}

let DATASPACE="stable";
if(typeof __ENV.DATASPACE !== 'undefined'){
	DATASPACE = __ENV.DATASPACE;
}

//Keycloak parameters
let ACCESS_TOKEN_URL= "https://dotstat-access-pp.oecd.org/auth/realms/OECD/protocol/openid-connect/token";
if(typeof __ENV.KEYCLOAK_AT_URL !== 'undefined'){
	ACCESS_TOKEN_URL = __ENV.KEYCLOAK_AT_URL;
}
let currentAccessToken = "";
let accessTokenExpiry ="";

//Keycloak credentials
let USERNAME="";
if(typeof __ENV.USERNAME !== 'undefined'){
	USERNAME = __ENV.USERNAME;
}

let PASSWORD="";
if(typeof __ENV.PASSWORD !== 'undefined'){
	PASSWORD = __ENV.PASSWORD;
}

//login to keycloak?
let getToken =true;
if(typeof __ENV.USERNAME === 'undefined' || typeof __ENV.PASSWORD === 'undefined'){
	getToken=false;
}

let INPUT_FILE = "./Resources/test-cases-data-imports-smoke-OECD-PP.json";
if(typeof __ENV.TEST_CASES_FILE !== 'undefined'){
	INPUT_FILE = __ENV.TEST_CASES_FILE;
}

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
//Open input files
for(let testCase in TEST_CASES){
	if(TEST_CASES[testCase].format ==="excel"){
		TEST_CASES[testCase].data = open(`./Resources/Data/${TEST_CASES[testCase].dataFile}`, "b");
		TEST_CASES[testCase].edd = open(`./Resources/Data/${TEST_CASES[testCase].eddFile}`, "b");
	}
}

//IMPORTANT! The time has to be adjusted depending on the amount of imports, to give enough time for all the imports to complete
let waitTimeInSeconds = TEST_CASES.length * 60;//total amount of usecases * 30 seconds

let importRate = new Rate('data_import_completed');
let importTrend = new Trend('data_import_time', true);

export let options = {
	setupTimeout: "10s",
	ext: {
		loadimpact: {
		  projectID: PID, //k6 CLOUD project id 
		  name: TEST_NAME
		}
	},
	thresholds: {
		"checks": ['rate>0.99'], // more than 99% success rate on import requests
		"data_import_completed": ['rate>0.90'], // more than 95% success rate of transactions imported
        "data_import_time{import_type:csv_small}": ["p(95)<10000"],//less than 10 seconds
        "data_import_time{import_type:csv_medium}": ["p(95)<70000"],//less than 70 seconds
        "data_import_time{import_type:csv_large}": ["p(95)<150000"],//less than 150 seconds
        "data_import_time{import_type:xml_small}": ["p(95)<10000"],//less than 10 seconds
        "data_import_time{import_type:xml_medium}": ["p(95)<70000"],//less than 70 seconds
        "data_import_time{import_type:xml_large}": ["p(95)<150000"],//less than 150 seconds
        "data_import_time{import_type:sdmx_small}": ["p(95)<10000"],//less than 10 seconds
        "data_import_time{import_type:sdmx_medium}": ["p(95)<70000"],//less than 70 seconds
        "data_import_time{import_type:sdmx_large}": ["p(95)<150000"],//less than 150 seconds
        "data_import_time{import_type:excel_extraSmall}": ["p(95)<5000"],//less than 5 seconds
        "data_import_time{import_type:excel_small}": ["p(95)<10000"],//less than 10 seconds
        "data_import_time{import_type:excel_medium}": ["p(95)<70000"],//less than 70 seconds
        "data_import_time{import_type:excel_large}": ["p(95)<150000"],//less than 150 seconds
		
        "data_import_time{datasetSize:extraSmall}": ["p(95)<150000"],
        "data_import_time{datasetSize:small}": ["p(95)<150000"],
        "data_import_time{datasetSize:medium}": ["p(95)<150000"],
        "data_import_time{datasetSize:large}": ["p(95)<150000"],
        "data_import_time{datasetSize:extraLarge}": ["p(95)<150000"],	
	},
	iterations: 1,
	vus: 1,

};

export function setup() {
	// 2. setup code
	
	//check that the transfer-service hostname is available
	let healhCheck = http.get(`${BASE_URL}/health`);
	if (healhCheck.status !== 200){
		fail(`Error: the transfer-service {${BASE_URL}/health} is not responding.`);
	}
	console.log(`Testing the transfer-service {${BASE_URL}} version ${healhCheck.json().service.details.version}`);
	
}

export default function() {
	var submitedRequests = [];

	TryToGetNewAccessToken();
	//Submit data import requests
	for(let testCase in TEST_CASES){
		let headers= {
			'Accept':'application/json',
			'Authorization': `Bearer ${currentAccessToken}`, 
		};
		
		var method = "/1.2/import/sdmxFile";
		let data= { 
			'dataspace': DATASPACE	
		};
		
		//Import from SDMX source
		if(TEST_CASES[testCase].format ==="sdmx" ){
			data.filepath= TEST_CASES[testCase].sdmxSource;
			//Workaround - K6 only supports multipart/from-data request if there is a file in the request. 
			data.file= http.file("", "dummyFile.csv");
			console.log(`Importing from url: ${data.filepath}`);
		}
		//Import from Excel
		else if(TEST_CASES[testCase].format ==="excel" ){
		    method = "/1.2/import/excel";
			data.eddFile  = http.file(TEST_CASES[testCase].edd, TEST_CASES[testCase].eddFile);
			data.excelFile = http.file(TEST_CASES[testCase].data, TEST_CASES[testCase].dataFile);
			console.log(`Importing excel file: ${TEST_CASES[testCase].eddFile}`);
		}
		//Import from CSV and XML
		else{
			data.filepath= TEST_CASES[testCase].FilePath;
			//Workaround - K6 only supports multipart/from-data request if there is a file in the request. 
			data.file= http.file("", "dummyFile.csv");
			console.log(`Importing from remote file: ${TEST_CASES[testCase].FilePath}`);
		}
		
		var res = http.post(`${BASE_URL}${method}`, data, {headers: headers});

		console.log(`import status:${res.status}`);
		
		check(res, {
			'is status 200': (r) => r.status === 200
		});
		 
		if(res.status ===200){
			console.log(`import message:${res.json().message}`);
			var transactionID = res.json().message.match(/\d+/g);
			submitedRequests.push({
				'ID': transactionID,
				'format': TEST_CASES[testCase].format,
				'size': TEST_CASES[testCase].size,
				'datasetSize': TEST_CASES[testCase].datasetSize,
			});
		}
		else if(res.status >= 500){
			console.log(`import message:${res.body}`);
		}
		else{
			console.log(`import message:${res.json().message}`);
		}
	}
	
	//Wait for the transfer-service to complete all imports
	sleep(waitTimeInSeconds);
	
	//Get new access token if current token has expired
	TryToGetNewAccessToken();
	
	//Get status of requests
	for(let request in submitedRequests){
		let data= { 
			'dataspace': DATASPACE,	
			'id': parseInt(submitedRequests[request].ID)
		};
		let headers= {
			'Accept':'application/json',
			'Authorization': 'Bearer ' + currentAccessToken, 
		};
		
		var res = http.post(
			`${BASE_URL}/1.2/status/request`, 
			data,  
			{headers: headers},
		);
			
		if(res.status===200){
			if(res.json().executionStatus==="Completed"){
				var actualTime = Date.parse(res.json().executionEnd) - Date.parse(res.json().executionStart);
				importTrend.add(actualTime, { import_type: `${submitedRequests[request].format}_${submitedRequests[request].size}` });
				importTrend.add(actualTime, { datasetSize: `${submitedRequests[request].datasetSize}` });
				
				//The import was completed
				importRate.add(true);
			}
			else{
				console.log(`Execution status:${res.json().executionStatus}`);
				//The import was not completed
				importRate.add(false);
			}
		
		}
	}
}
