import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';

export function TryToGetNewAccessToken(){
	
    if (!accessTokenExpiry || !currentAccessToken) {
        console.log("Token or expiry date are missing");
    } else if (accessTokenExpiry <= new Date().getTime()) {
        console.log("Token is expired");
    } else {
        getToken = false;
        console.log("Token and expiry date are all good");
    }

    if (getToken === true) {
        //get the access token first
		let data= { 
			'grant_type': "password", 
			'client_id': "app", 
			'scope': "openid", 
			'username': USERNAME, 
			'password': PASSWORD
		};
		//Get new access token
		let res = http.post(ACCESS_TOKEN_URL, data, {headers: {'Accept':'application/json'}});
		
        var responseJson = res.json();
        currentAccessToken = responseJson.access_token;
        var expiryDate = new Date();
        expiryDate.setSeconds(
          expiryDate.getSeconds() + responseJson.expires_in
        );
        accessTokenExpiry = expiryDate.getTime();
       
        sleep(1);
        //console.log(currentAccessToken);
    }
	
	if(getToken && !currentAccessToken)
		fail("Could not login, please check the USERNAME and PASSWORD");
}